Computer A: nc -l -p 1234 > filename.txt

Computer B: nc server.com 1234 < filename.txt



Init the target listening to the port. AKA receiver end

nc -vl 44444 > pick_desired_name_for_received_file

Send the file to the target. AKA sender end

nc -n TargetIP 44444 < /path/to/file/you/want/to/send

#./create-green.sh /bin/{ls,cat,echo,pwd,bash,sh} /usr/bin/vi /etc/hosts
#!/bin/bash

CHROOT='/srv/green'
mkdir $CHROOT

for i in $( ldd $* | grep -v dynamic | cut -d " " -f 3 | sed 's/://' | sort | uniq )
  do
    cp --parents $i $CHROOT
  done

# ARCH amd64
if [ -f /lib64/ld-linux-x86-64.so.2 ]; then
   cp --parents /lib64/ld-linux-x86-64.so.2 /$CHROOT
fi

# ARCH i386
if [ -f  /lib/ld-linux.so.2 ]; then
   cp --parents /lib/ld-linux.so.2 /$CHROOT
fi

echo "Chroot jail is ready. To access it execute: chroot $CHROOT"

#add user:
#useradd username
#usermod --shell /bin/bash username

#addgroup chrootjail
#adduser username chrootjail
#or 
#usermod -a -G chrootjail username
#usermod -a -G dialout username (arduino example)

#add to /etc/sshd_config
#Match group chrootjail
#            ChrootDirectory /srv/chroot/


